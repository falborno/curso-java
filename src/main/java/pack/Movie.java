package pack;

public class Movie {
    
    private String title, gender, year;
    
    public Movie(String title, String gender, String year){   
        this.title = "Titulo: " + title;
        this.gender = " /// Género: " + gender;
        this.year = " /// Año: " + year;   
    }
    
    public String getTitle(){ 
        return this.title;
    }
    public String getGender(){ 
        return this.gender;
    }
    public String getYear(){
        return this.year;
    } 
    @Override // Sugerencia de NetBeans para realizar polimorfismo.
    public String toString(){
        return getTitle()+getGender()+getYear();        
    }    
    
}
package pack;

import java.util.ArrayList;

public class CinemaListing {
    
    ArrayList<Movie>  myList = new ArrayList();

    public CinemaListing() {
        
        //Hola soy un constructor

    }
    
    void playMovie(){

        for(int i = 0; i < myList.size(); i++){
            
            System.out.println(myList.get(i).toString());
            
        }    
    }
    
    void loadMovie(Movie arg){
               
        myList.add(arg);
                           
    }

    public static void main(String[] args){
        
        Movie obj1 = new Movie("Top Secret!","Comedia","1984");
        Movie obj2 = new Movie("The Truman Show","Drama","1998");
        Movie obj3 = new Movie("Wind River","Drama","2017");
        Movie obj4 = new Movie("Avangers: Infinity War","Acción","2018");
        
        CinemaListing ob1 = new CinemaListing();
        
        ob1.loadMovie(obj1);
        ob1.loadMovie(obj2);
        ob1.loadMovie(obj3);
        ob1.loadMovie(obj4);
        
        ob1.playMovie();

    }
    
}